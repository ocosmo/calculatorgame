import React, { useReducer } from 'react';
import './App.css';
import { nbrOfCorrectAnswers } from './utils/CheckAnswers';
import Question from './components/Question';
import Answer from './components/Answer';
import Advance from './components/Advance';
import Header from './components/Header';
import gameReducer from './reducers/GameReducer';
import actions from './reducers/actions';
import {nbrOfLevels} from './domain/LevelGuide';

function App() {
	const initialState = {
		questions: [],
		currentQuestion: undefined,
		currentQuestionIndex: 0,
		currentAnswer: '',
		level: 0,
		numberOfQuestions: 2,
		readyForNextLevel: true
	};

	const [state, dispatch] = useReducer(gameReducer, initialState);
	const { currentQuestion, currentAnswer, questions, currentQuestionIndex, numberOfQuestions, level, readyForNextLevel } = state;


	const nextQuestionOrLevel = () => {
		if (currentQuestionIndex === numberOfQuestions - 1) {
			const required = Math.ceil(numberOfQuestions * 0.7);
			if (nbrOfCorrectAnswers(questions) >= required) {
				dispatch({ type: actions.SET_QUESTIONS, value: [] });
				return;
			}
		}
		nextQuestion();
	}

	const nextQuestion = () => {
		dispatch({ type: actions.NEXT_QUESTION });
	}

	const nextLevel = () => {
		dispatch({ type: actions.NEXT_LEVEL });
	}

	const onFormSubmit = (event) => {
		event.preventDefault();
		const form = event.target;
		const input = form.elements['answerInput'];
		const val = input.value !== undefined ? parseInt(input.value, 10) : null;
		if (val === currentQuestion.correct) {
			dispatch({ type: actions.SET_CURRENT_AS_CORRECT });
			window.setTimeout(() => { nextQuestionOrLevel(); input.value = undefined; }, 1200);
		} else {
			dispatch({ type: actions.SET_CURRENT_AS_INCORRECT, value: val });
			window.setTimeout(() => { dispatch({ type: actions.SET_CURRENT_AS_NORESULT }); input.value = undefined }, 1200);
		}
	}

	const handleReset = () => {
		dispatch({ type: actions.RESET_GAME });
	}

	if (questions?.length) {
		return (
			<div className="container">
				<Header questions={questions} level={level} handleReset={handleReset}></Header>
				<div className="game">
					<Question question={currentQuestion} />
					<Answer onFormSubmit={onFormSubmit} question={currentQuestion} answer={currentAnswer} />
				</div>
			</div>
		)
	}
	const levels = nbrOfLevels() ;
	if (level === levels) {
		return (
			<div className="container">
				<Header questions={questions} level={level}></Header>
				KLAR!
				<button autoFocus onClick={handleReset} type="button">Starta om!</button>
			</div>
		);
	}
	return (
		<div className="container">
			<Header questions={questions} level={level}></Header>
			<Advance readyForNextLevel={readyForNextLevel} level={level} handleClick={nextLevel}></Advance>
		</div>
	);
}

export default App;
