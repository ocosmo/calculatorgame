import React from "react";

export default function advance(props) {

	let advance = false;
	if (props.readyForNextLevel && props.level > 0) {
		advance = true;
	}

	return (
		<div className="advance">
			<button autoFocus onClick={props.handleClick} type="button">{advance ? 'Nästa Nivå!' : 'Starta'}</button>
		</div>
	)
}