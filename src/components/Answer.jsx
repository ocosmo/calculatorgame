import React from "react";

export default function Answer(props) {

	return (
		<form className="answer" onSubmit={props.onFormSubmit}>
			<input autoFocus className="answer__input" type="number" name="answerInput" />
			<input type="submit" />
			<Correct question={props.question} />
			<Guesses incorrect={props.question.incorrectAnswers} />
		</form>
	);
}

const Guesses = (props) => {
	if (props.incorrect.length == 0) return null;
	return (
		<div className="guesses">
			<ul>
			{ props.incorrect.map((value, index) => {
				return <li key={`incrg-${index}`}>{value}</li>
			})}
			</ul>
		</div>);
}

export function Correct(props) {
	if (!props.question || props.question.result === undefined) {
		return null;
	}

	const renderResult = () => {
		if (props.question.result === true) {
			return <span className="marker marker--correct">✓</span>
		} else if (props.question.result === false){
			return <span className="marker marker--incorrect">X</span>
		}
	}

	return renderResult();
}