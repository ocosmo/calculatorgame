import React from "react";
import { nbrOfCorrectAnswers } from '../utils/CheckAnswers';

export default function header(props) {
	const { questions, level } = props;

	return (
		<div className="header">
			{props.level > 0 &&
				<>
					<button onClick={props.handleReset} type="button">Starta om</button>
					<div className="header__info">
						<div className="header__level">
							Level: {level}</div>
						<div className="header__steps_info">
							{nbrOfCorrectAnswers(questions) + 1 } / {questions.length}
						</div>
					</div>
				</>
			}
		</div>
	);
}