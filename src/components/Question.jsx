import React from "react";

export default function Question(props) {
	const question = props.question;

	if (!question) {
		return null;
	}

	const translationMap = {
		add: '+',
		subtract: '-'
	}

	const parts = [];
	question.members.forEach((mbr, idx) => {
		parts.push(mbr);
		parts.push(translationMap[question.func[idx]]);
	});

	return (
		<div>
			<span>{parts}</span>
			<span>=</span>
		</div>
	);
}