export const actions = [
	'add',
	'subtract'
];

const levelGuide = {
	1: { maxMembers: 2, maxValue: 20, actions: 'subtract', onlyPositive: true },
	// 2: { maxMembers: 3, maxValue: 10,  actions: actions[0] },
	// 3: { maxMembers: 2, maxValue: 10, actions: actions[1] },
	// 4: { maxMembers: 2, maxValue: 20, actions: 'all' }
}

export function nbrOfLevels() {
	return Object.keys(levelGuide).length;
}


export default levelGuide;