import actions from './actions';
import generateQuestions from '../utils/GenerateQuestions';

export default function gameReducer(state, action) {
	switch (action.type) {
		case actions.SET_QUESTIONS:
			return { ...state, questions: action.value };
		case actions.NEXT_LEVEL:
			const newQuestions = generateQuestions(state.level + 1, state.numberOfQuestions);
			return {
				...state,
				currentAnswer: undefined,
				currentQuestionIndex: 0,
				questions: newQuestions,
				currentQuestion: newQuestions[0],
				level: state.level + 1
			};
		case actions.NEXT_QUESTION:
			return {
				...state,
				currentAnswer: undefined,
				currentQuestion: state.questions[state.currentQuestionIndex + 1],
				currentQuestionIndex: state.currentQuestionIndex + 1
			};
		case actions.SET_CURRENT_AS_CORRECT:
			return updateResult(true, state);
		case actions.SET_CURRENT_AS_INCORRECT:
			return updateResult(false, state, action.value);
		case actions.SET_CURRENT_AS_NORESULT:
			return updateResult(undefined, state);
		case actions.NEW_GAME:
			return {
				...state,
				currentQuestion: action.value[state.currentQuestionIndex],
				questions: action.value
			};
		case actions.RESET_GAME:
			return {
				...state,
				level: 0,
				questions: [],
				currentAnswer: undefined,
				currentQuestion: undefined,
				currentQuestionIndex: 0,
			};
		default:
			return state;
	}
}

const updateResult = (result, state, value) => {
	let currentQuestion = { ...state.currentQuestion, result };
	if (value != undefined) {
		currentQuestion.incorrectAnswers.push(value);
	}
	const questions = state.questions;
	questions[state.currentQuestionIndex] = { ...currentQuestion };
	return { ...state, currentQuestion, questions };
}