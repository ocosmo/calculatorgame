export function nbrOfCorrectAnswers(questions) {
	const correct = questions.filter((q => q.result));
	return correct.length;
}