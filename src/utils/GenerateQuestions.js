import levelGuide, { actions } from '../domain/LevelGuide';

export default function generateQuestions(currentLevel, nbrOfQuestions = 5) {
	const questions = [];
	const levelInfo = levelGuide[currentLevel];
	while (questions.length < nbrOfQuestions) {
		let nbrOfMembers = randomIntFromInterval(2, levelInfo.maxMembers);
		let members = [];
		let func = [];
		for (let mbr = 0; mbr < nbrOfMembers; mbr++) {
			members.push(randomIntFromInterval(0, levelInfo.maxValue));
			if(mbr < nbrOfMembers - 1) {
				const fn = levelInfo.actions !== 'all' ? levelInfo.actions : actions[randomIntFromInterval(0, 1)];
				func.push(fn);
			}
		}
		const correct = members.reduce((prev, current, index) => func[index-1] === 'add' ? prev + current : prev - current);
		if (!levelInfo.onlyPositive || correct > 0 ) {
			questions.push({ members, func, correct, result: undefined, incorrectAnswers: [] })
		}
	}
	return questions;
}

export function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}